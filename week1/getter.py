import pandas as pd
from pathlib import Path
from datetime import datetime, date, timedelta
import pytz
from urllib.request import urlopen
import json
import pandas_market_calendars as mcal
import sqlalchemy as db
from date_utils import datetime2week, week2datetimes, week2nysedays


class Getter:
    def __init__(self, db_file):
        self.base_url = "https://financialmodelingprep.com/api/v3"
        self.url_extensions_dict = {
            'price': '/historical-price-full/',
            'profile': '/company/profile/',
            'income-statement': '/financials/income-statement/',
            'balance-sheet-statement': '/financials/balance-sheet-statement/',
            'cash-flow-statement': '/financials/cash-flow-statement/',
            'financial-ratios': '/financial-ratios/'
        }
        self.lookup_name_dict = {
            'price': 'historical',
            'income-statement': 'financials',
            'balance-sheet-statement': 'financials',
            'cash-flow-statement': 'financials'
        }
        self.all_fields_dict = {
            'price': ['date', 'open', 'high', 'low', 'close', 'adjClose', 'volume',
                      'unadjustedVolume', 'change', 'changePercent', 'vwap', 'label',
                      'changeOverTime'],
            'profile': ['price', 'beta', 'volAvg', 'mktCap', 'lastDiv', 'range',
                        'changes', 'changesPercentage', 'companyName', 'exchange',
                        'industry', 'website', 'description', 'ceo', 'sector', 'image'],
            'income-statement': ['date', 'Revenue', 'Revenue Growth', 'Cost of Revenue', 'Gross Profit', 'R&D Expenses',
                                 'SG&A Expense', 'Operating Expenses', 'Operating Income', 'Interest Expense',
                                 'Earnings before Tax', 'Income Tax Expense', 'Net Income - Non-Controlling int',
                                 'Net Income - Discontinued ops', 'Net Income', 'Preferred Dividends', 'Net Income Com',
                                 'EPS', 'EPS Diluted', 'Weighted Average Shs Out', 'Weighted Average Shs Out (Dil)',
                                 'Dividend per Share', 'Gross Margin', 'EBITDA Margin', 'EBIT Margin', 'Profit Margin',
                                 'Free Cash Flow margin', 'EBITDA', 'EBIT', 'Consolidated Income',
                                 'Earnings Before Tax Margin', 'Net Profit Margin'],
            'balance-sheet-statement': ['date', 'Cash and cash equivalents', 'Short-term investments',
                                        'Cash and short-term investments', 'Receivables', 'Inventories',
                                        'Total current assets', 'Property, Plant & Equipment Net',
                                        'Goodwill and Intangible Assets', 'Long-term investments', 'Tax assets',
                                        'Total non-current assets', 'Total assets', 'Payables', 'Short-term debt',
                                        'Total current liabilities', 'Long-term debt', 'Total debt', 'Deferred revenue',
                                        'Tax Liabilities', 'Deposit Liabilities', 'Total non-current liabilities',
                                        'Total liabilities', 'Other comprehensive income',
                                        'Retained earnings (deficit)', 'Total shareholders equity', 'Investments',
                                        'Net Debt', 'Other Assets', 'Other Liabilities'],
            'cash-flow-statement': ['date', 'Depreciation & Amortization', 'Stock-based compensation',
                                    'Operating Cash Flow', 'Capital Expenditure', 'Acquisitions and disposals',
                                    'Investment purchases and sales', 'Investing Cash flow',
                                    'Issuance (repayment) of debt', 'Issuance (buybacks) of shares',
                                    'Dividend payments', 'Financing Cash Flow', 'Effect of forex changes on cash',
                                    'Net cash flow / Change in cash', 'Free Cash Flow', 'Net Cash/Marketcap']
        }
        self.standard_fields_dict = {
            'price': ['date', 'open', 'high', 'low', 'close', 'adjClose', 'volume', 'unadjustedVolume', 'change',
                      'changePercent', 'vwap', 'label', 'changeOverTime'],
            'profile': ['price', 'beta', 'volAvg', 'mktCap', 'lastDiv', 'industry', 'sector'],
            'income-statement': ['date', 'Revenue', 'Revenue Growth', 'Cost of Revenue', 'Gross Profit', 'R&D Expenses',
                                 'SG&A Expense', 'Operating Expenses', 'Operating Income', 'Interest Expense',
                                 'Earnings before Tax', 'Income Tax Expense', 'Net Income - Non-Controlling int',
                                 'Net Income - Discontinued ops', 'Net Income', 'Preferred Dividends', 'Net Income Com',
                                 'EPS', 'EPS Diluted', 'Weighted Average Shs Out', 'Weighted Average Shs Out (Dil)',
                                 'Dividend per Share', 'Gross Margin', 'EBITDA Margin', 'EBIT Margin', 'Profit Margin',
                                 'Free Cash Flow margin', 'EBITDA', 'EBIT', 'Consolidated Income',
                                 'Earnings Before Tax Margin', 'Net Profit Margin'],
            'balance-sheet-statement': ['date', 'Cash and cash equivalents', 'Short-term investments',
                                        'Cash and short-term investments', 'Receivables', 'Inventories',
                                        'Total current assets', 'Property, Plant & Equipment Net',
                                        'Goodwill and Intangible Assets', 'Long-term investments', 'Tax assets',
                                        'Total non-current assets', 'Total assets', 'Payables', 'Short-term debt',
                                        'Total current liabilities', 'Long-term debt', 'Total debt',
                                        'Deferred revenue', 'Tax Liabilities', 'Deposit Liabilities',
                                        'Total non-current liabilities', 'Total liabilities',
                                        'Other comprehensive income', 'Retained earnings (deficit)',
                                        'Total shareholders equity', 'Investments', 'Net Debt', 'Other Assets',
                                        'Other Liabilities'],
            'cash-flow-statement': ['date', 'Depreciation & Amortization', 'Stock-based compensation',
                                    'Operating Cash Flow', 'Capital Expenditure', 'Acquisitions and disposals',
                                    'Investment purchases and sales', 'Investing Cash flow',
                                    'Issuance (repayment) of debt', 'Issuance (buybacks) of shares',
                                    'Dividend payments', 'Financing Cash Flow', 'Effect of forex changes on cash',
                                    'Net cash flow / Change in cash', 'Free Cash Flow', 'Net Cash/Marketcap']
        }
        self.numeric_fields_dict = {
            'price': ['open', 'high', 'low', 'close', 'adjClose', 'volume', 'unadjustedVolume', 'change',
                      'changePercent', 'vwap', 'changeOverTime'],
            'profile': ['price', 'beta', 'volAvg', 'mktCap', 'lastDiv', 'range', 'changes', 'changesPercentage'],
            'income-statement': ['Revenue', 'Revenue Growth', 'Cost of Revenue', 'Gross Profit', 'R&D Expenses',
                                 'SG&A Expense', 'Operating Expenses', 'Operating Income', 'Interest Expense',
                                 'Earnings before Tax', 'Income Tax Expense', 'Net Income - Non-Controlling int',
                                 'Net Income - Discontinued ops', 'Net Income', 'Preferred Dividends', 'Net Income Com',
                                 'EPS', 'EPS Diluted', 'Weighted Average Shs Out', 'Weighted Average Shs Out (Dil)',
                                 'Dividend per Share', 'Gross Margin', 'EBITDA Margin', 'EBIT Margin', 'Profit Margin',
                                 'Free Cash Flow margin', 'EBITDA', 'EBIT', 'Consolidated Income',
                                 'Earnings Before Tax Margin', 'Net Profit Margin'],
            'balance-sheet-statement': ['Cash and cash equivalents', 'Short-term investments',
                                        'Cash and short-term investments', 'Receivables', 'Inventories',
                                        'Total current assets', 'Property, Plant & Equipment Net',
                                        'Goodwill and Intangible Assets', 'Long-term investments', 'Tax assets',
                                        'Total non-current assets', 'Total assets', 'Payables', 'Short-term debt',
                                        'Total current liabilities', 'Long-term debt', 'Total debt',
                                        'Deferred revenue', 'Tax Liabilities', 'Deposit Liabilities',
                                        'Total non-current liabilities', 'Total liabilities',
                                        'Other comprehensive income', 'Retained earnings (deficit)',
                                        'Total shareholders equity', 'Investments', 'Net Debt', 'Other Assets',
                                        'Other Liabilities'],
            'cash-flow-statement': ['Depreciation & Amortization', 'Stock-based compensation',
                                    'Operating Cash Flow', 'Capital Expenditure', 'Acquisitions and disposals',
                                    'Investment purchases and sales', 'Investing Cash flow',
                                    'Issuance (repayment) of debt', 'Issuance (buybacks) of shares',
                                    'Dividend payments', 'Financing Cash Flow', 'Effect of forex changes on cash',
                                    'Net cash flow / Change in cash', 'Free Cash Flow', 'Net Cash/Marketcap']

        }
        self.is_Sunday, self.most_recent_close, self.days_open_last_week = self.check_nyse()
        if not self.is_Sunday:
            db_file = db_file.replace('.db', '_test.db')
        print(db_file)
        self.engine = db.create_engine(db_file)
        print('have created engine')

    def get_time(self):
        '''
        no longer being used (self.check_nyse() instead)
        need to note datetime lessons somewhere before deleting this (and incorporate learnings in get_nyse())
        '''
        now = datetime.now()
        NewYork_now = now.astimezone(pytz.timezone('US/Eastern'))
        NewYork_date = NewYork_now.date()
        print('NY now', NewYork_now)
        print('NY date', NewYork_date)
        print('NY hour', NewYork_now.hour)
        print('NY weekday (0 -> Sunday)', NewYork_now.weekday())
        print('NY now', NewYork_now.strftime('%Y-%m-%d, %H:%M, %a, %w'))

        # get last 10 days of historical data, and see what is the most recent date available
        from_date = NewYork_date + timedelta(days=-10)
        url = self.base_url + '/historical-price-full/AAPL?from=%s' % from_date
        print(url)
        data = self.get_from_url(url)
        today = data['historical'][-1]['date']
        print(data)
        print(today)

    def check_nyse(self):
        '''
        :return:
        is_Sunday: Boolean, will update db for real if True
        recent_week_close_ymd: yyyy-mm-dd for the day the NYSE closed last week
        (may have dodgy value if today isn't Sunday in Australia)
        '''
        nyse = mcal.get_calendar('NYSE')
        today = datetime.now().date()
        is_Sunday = today.weekday() == 6
        fortnight_ago = (today - timedelta(days=14)).strftime('%Y-%m-%d')  # string
        recent_open_dates = nyse.schedule(fortnight_ago, today)  # df
        recent_open_dates = [d.to_pydatetime().date() for d in recent_open_dates.index]  # list of dates
        recent_week_close_ymd = sorted([(d.weekday(), d.strftime('%Y-%m-%d')) for d in recent_open_dates])[-1][1]
        print(is_Sunday, recent_week_close_ymd)
        days_open_last_week = sorted([str(d) for d in recent_open_dates
                                      if datetime2week(d) == datetime2week(recent_week_close_ymd)])
        return is_Sunday, recent_week_close_ymd, days_open_last_week

    def get_all_symbols(self, subset=None):
        # returns a list of all stock codes, etc., in the fmp database
        url = 'https://financialmodelingprep.com/api/v3/company/stock/list'
        symbols = self.find_in_json(json.loads(urlopen(url).read().decode('utf-8')), 'symbol')
        if type(subset) == int:
            symbols = symbols[:subset]
        elif type(subset) == list:
            symbols = [s for s in subset if s in symbols]
        print('gotten', len(symbols), 'symbols')
        return symbols

    def get_from_url(self, url):
        response = urlopen(url)
        json_data = response.read().decode('utf-8')
        data = json.loads(json_data)
        return data

    def get_most_recent_close(self, symbol):
        url = self.base_url + '/stock/real-time-price/%s' % symbol
        return self.get_from_url(url)

    def get_data(self, symbols, data_type, which_fields, from_date=None, to_date=None):
        url = self.base_url + self.url_extensions_dict[data_type]
        if which_fields == 'all':
            fields = self.all_fields_dict[data_type]
        elif which_fields == 'standard':
            fields = self.standard_fields_dict[data_type]
        elif which_fields == 'investigative':
            self.check(url, symbols, from_date, to_date)
        else:
            fields = which_fields  # assume a list of fields wanted has been passed
        numeric_fields = [f for f in fields if f in self.numeric_fields_dict[data_type]]
        ddict = {field: [] for field in ['symbol'] + fields}
        for symbol in symbols:
            full_url = url + symbol
            if from_date or to_date:
                full_url += "?"
            if from_date:
                full_url += "from=%s" % from_date
            if from_date and to_date:
                full_url += "&"
            if to_date:
                full_url += 'to=%s" % to_date'
            print(full_url)
            data = json.loads(urlopen(full_url).read().decode('utf-8'))
            print('data', data)
            if data_type in self.lookup_name_dict:
                for datum in data[self.lookup_name_dict[data_type]]:
                    ddict['symbol'].append(symbol)
                    for field in fields:
                        ddict[field].append(datum[field])
            elif data_type == 'profile':
                ddict['symbol'].append(symbol)
                for field in fields:
                    ddict[field].append(data['profile'][field])
            elif data_type == 'financial-ratios':
                for i, item in enumerate(data['ratios']):
                    for key in item:
                        if type(item[key]) == dict:
                            for k in item[key]:
                                dict[k].append(item[key][k])
                        else:
                            ddict[key].append(item[key])



            else:
                print('unknown data_type', data_type)
                raise Exception

        df = pd.DataFrame(ddict)
        if data_type == 'financial-ratios':
            numeric_fields = list(ddict.keys()).remove('date')
        for field in numeric_fields:
            df[field] = pd.to_numeric(df[field], errors='coerce')
        # check for NaN
        check = df[numeric_fields].sum()
        if any(check.isna()):
            print('NaN detected')
            print(check)
            raise Exception
        return df

    def check(self, url, symbols, from_date, to_date):
        '''
        investigate what's out there
        :param url:
        :param symbols:
        :param from_date:
        :param to_date:
        :return:
        '''
        for symbol in symbols:
            full_url = url + symbol
            if from_date or to_date:
                full_url += "?"
            if from_date:
                full_url += "from=%s" % from_date
            if from_date and to_date:
                full_url += "&"
            if to_date:
                full_url += 'to=%s" % to_date'
            print(full_url)
            data = json.loads(urlopen(full_url).read().decode('utf-8'))
            print('data', data)

            # more mutable code follows

            # for most
            # print(len(data['financials']))
            # for i, financial in enumerate(data['financials']):
            #     print(i, financial)
            #     for key in financial:
            #         print(key, financial[key])
            #     print(list(financial.keys()))
            #     raise Exception

            # for balance sheet ratios
            print('data[ratios]', data['ratios'])
            print(len(data['ratios']))
            first = data['ratios'][0]
            for key in first:
                print(key)
                if type(first[key]) == dict:
                    for k in first[key]:
                        print('\t', k, first[key][k])
                else:
                    print(key, first[key])


            raise Exception
            for i, ratio_set in enumerate(data['ratios']):
                print('keys', ratio_set.keys())
                print(len(list(ratio_set.keys())))
            raise Exception

    def to_db(self, df, table, drop_table=False):
        connection = self.engine.connect()
        if drop_table:
            connection.execute("drop table if exists %s" % table)
        columns = list(df.columns)
        week = datetime2week(self.most_recent_close)
        df['week'] = week
        columns = ['symbol', 'week'] + columns[1:]  # primary keys at start
        print(table, columns)
        print('df:', list(df[columns]))
        df = df[columns]
        print(list(df.columns))
        df.to_sql(table, connection, if_exists='append', index=False)

        if self.is_Sunday or True:
            print('days_open_last_week', self.days_open_last_week)
            # connection.execute("drop table if exists dates")
            connection.execute("create table if not exists dates (date text, week int, weekday text, open int);")
            dates = pd.read_sql('dates', connection)
            new_weeks = []
            if len(dates['week']) == 0:
                new_weeks = [week]
            else:
                new_weeks = range(max(dates['week']), week + 1)
            for week in new_weeks:
                days = week2datetimes(week)
                open_days = week2nysedays(week)
                for day in days:
                    sql = "insert into %s (date, week, weekday, open) values ('%s', %d, '%s', %d)" % \
                          ("dates", day.strftime('%Y-%m-%d'), week, day.strftime("%a"), day in open_days)
                    print(sql)
                    connection.execute(sql)

    def find_in_json(self, obj, key):
        '''
        Scan the json file to find the value of the required key.
        Input: json file
               required key
        Output: value corresponding to the required key
        '''
        # Initialize output as empty
        arr = []

        def extract(obj, arr, key):
            '''
            Recursively search for values of key in json file.
            '''
            if isinstance(obj, dict):
                for k, v in obj.items():
                    if isinstance(v, (dict, list)):
                        extract(v, arr, key)
                    elif k == key:
                        arr.append(v)
            elif isinstance(obj, list):
                for item in obj:
                    extract(item, arr, key)
            return arr

        results = extract(obj, arr, key)
        return results


if __name__ == '__main__':
    # symbols = get_all_symbols()
    symbols = ['BA']
    scratch_p = Path('../../scratch')

    getter = Getter('sqlite:///../../sqlite/fmp.db')
    getter.check_nyse()
    symbols = getter.get_all_symbols(subset=symbols)

    # price = getter.get_data(symbols, 'price', 'standard', from_date='2020-03-01')
    # getter.to_db(price, 'price', drop_table=True)
    # price.to_csv(scratch_p / 'price.csv', index=False)
    #
    # profile = getter.get_data(symbols, 'profile', 'standard', from_date='2020-03-01')
    # getter.to_db(profile, 'profile', drop_table=True)
    # profile.to_csv(scratch_p / 'profile.csv', index=False)

    # income_statement = getter.get_data(symbols, 'income-statement', 'standard')
    # getter.to_db(income_statement, 'income_statement', drop_table=True)
    # income_statement.to_csv(scratch_p / 'income-statement.csv', index=False)

    # balance_sheet_statement = getter.get_data(symbols, 'balance-sheet-statement', 'standard')
    # getter.to_db(balance_sheet_statement, 'balance_sheet_statement', drop_table=True)
    # balance_sheet_statement.to_csv(scratch_p / 'balance-sheet-statement.csv', index=False)

    # cash_flow_statement = getter.get_data(symbols, 'cash-flow-statement', 'standard')
    # getter.to_db(cash_flow_statement, 'cash_flow_statement', drop_table=True)
    # cash_flow_statement.to_csv(scratch_p / 'cash-flow-statement.csv', index=False)

    financial_ratios = getter.get_data(symbols, 'financial-ratios', 'standard')