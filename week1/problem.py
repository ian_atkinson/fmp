from datetime import date
import sqlalchemy as db
import pandas as pd
from datetime import date, timedelta
from date_utils import datetime2week, week2datetimes, week2nysedays, datetime2day

today = date.today()
print(today)


class What:
    def __init__(self):
        self.today = date.today()
        print(self.today)


class Getter:
    def __init__(self, db_file):
        self.base_url = "https://financialmodelingprep.com/api/v3"
        today = date.today()
        print(today)  # inexplicable error
        self.is_Sunday = (today.weekday() == 6)
        if not self.is_Sunday:
            db_file = db_file.replace('.db', '_test.db')
            self.week = datetime2week(date.today())
        else:
            self.week = datetime2week(date.today()) - 1
        print(db_file)
        self.engine = db.create_engine(db_file)
        connection = self.engine.connect()
        connection.execute(
            "create table if not exists dates (day int, date text, week int, weekday text, open int, primary key (day));")
        dates = pd.read_sql('dates', connection)
        if len(dates['week']) == 0:
            new_weeks = [self.week]
        else:
            new_weeks = range(max(dates['week']), self.week + 1)
        for week in new_weeks:
            dates = week2datetimes(week)
            open_days = week2nysedays(week)
            for dater in dates:
                day = datetime2day(dater)
                sql = "insert into %s (day, date, week, weekday, open) values (%d, '%s', %d, '%s', %d)" % \
                      ("dates", day, dater.strftime('%Y-%m-%d'), week, dater.strftime("%a"), dater in open_days)
                print(sql)
                connection.execute(sql)


if __name__ == '__main__':
    w = What()
    g = Getter('sqlite:///../../sqlite/problem.db')