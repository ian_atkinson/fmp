import pandas_market_calendars as mcal
import datetime
from date_utils import week2nysedays

# https://github.com/rsheftel/pandas_market_calendars

print(mcal.get_calendar_names())  # humpf, the ASX isn't included

nyse = mcal.get_calendar('NYSE')
print(nyse.schedule('2019-12-20', '2020-06-20'))
for a in sorted(dir(nyse)):
    if not a.startswith('_'):
        print(a, getattr(nyse, a))
print()
print(nyse.holidays())
print(sorted(nyse.adhoc_holidays)[-10:])
schedule = nyse.schedule('2019-12-20', '2020-06-20')
print(type(schedule))
print(schedule.index)
recent = [str(d).split(' ')[0] for d in schedule.index if str(d) < '2020-03-28']
print(recent[-5:])
print(schedule.index[0], type(schedule.index[0]))
# print(schedule.index[0].to_datetime())
# above fails, see https://pandas.pydata.org/pandas-docs/version/0.21/generated/pandas.Timestamp.to_datetime.html
print(schedule.index[0].to_pydatetime())  # awful, but this is recommended

print()
days = week2nysedays(1981)
print(days)
print(len(days))