from datetime import date
from functions import find_in_json
import pandas as pd
from urllib.request import urlopen
import json
from pathlib import Path

def get_all_symbols():
    url = 'https://financialmodelingprep.com/api/v3/company/stock/list'
    response = urlopen(url)
    data = response.read().decode('utf-8')
    symbols_json = json.loads(data)
    symbols = find_in_json(symbols_json, 'symbol')
    return symbols


def get_price_history(symbols, from_date=None, to_date=None):
    '''
    symbols: list of stock symbols to get data for 
    from_date, to_date: date range to get data for
    '''
    rval = []
    base_url = "https://financialmodelingprep.com"
    df = pd.DataFrame()
    for symbol in symbols:
        # url = (base_url + "/api/v3/stock/real-time-price/%s" % symbol)  # current price
        # url = (base_url + "/api/v3/historical-price-full/%s" % symbol)  # 5 years history
        # 'symbol': 'AAPL'
        # 'historical': [{'date'(YYYY-MM-DD), 'open', 'high', 'low', 'close', 'adjClose', 'volume', 'unadjustedVolume',
        #                   'change', 'changePercent', 'vwap', 'label'(March 06, 15), 'changeOverTime} ...]
        # url = (base_url + "/api/v3/historical-price-full/%s" % symbol + "?serietype=line") # ~30 years price-only
        # 'symbol;: 'AAPL',
        # 'historical': [{'date'(YYYY-MM-DD), 'close'} ...]
        # url = (base_url + "/api/v3/historical-price-full/%s?from=%s&to=%s" % (
        #     symbol, from_date.strftime('%Y-%m-%d'), to_date.strftime('%Y-%m-%d')))  # as before with date limit
        url = base_url + "/api/v3/historical-price-full/%s" % symbol
        if from_date or to_date:
            url += "?"
        if from_date:
            url += "from=%s" % from_date.strftime('%Y-%m-%d')
        if from_date and to_date:
            url += "&"
        if to_date:
            url += "to=%s" % to_date.strftime('%Y-%m-%d')
        print(url)
        response = urlopen(url)
        data = response.read().decode("utf-8")
        rval.append(json.loads(data))
        historical = json.loads(data)['historical']
        # print(historical)
        data = pd.DataFrame({'symbol': [symbol] * len(historical),
                             'date': [d['date'] for d in historical],
                             'open': [d['open'] for d in historical],
                             'high': [d['high'] for d in historical],
                             'low': [d['low'] for d in historical],
                             'close': [d['close'] for d in historical],
                             'adjClose': [d['adjClose'] for d in historical],
                             'volume': [d['volume'] for d in historical],
                             'unadjustedVolume': [d['unadjustedVolume'] for d in historical],
                             'change': [d['change'] for d in historical],
                             'changePercent': [d['changePercent'] for d in historical],
                             'vwap': [d['vwap'] for d in historical],
                             'label': [d['label'] for d in historical],
                             'changeOverTime': [d['changeOverTime'] for d in historical]
                             })
        df = df.append(data, ignore_index=True)
    return df


def get_profile(symbols, fields='standard'):
    df = pd.DataFrame()
    if fields == 'standard':
        fields = ['price', 'beta', 'volAvg', 'mktCap', 'lastDiv', 'industry', 'sector']
    elif fields == 'all':
        fields = ['price', 'beta', 'volAvg', 'mktCap', 'lastDiv', 'range', 'changes', 'changesPercentage',
                  'companyName', 'exchange', 'industry', 'website', 'description', 'ceo', 'sector', 'image']
    else:
        pass  # fields is already a list of what the calling code wants
    numeric_fields = [f for f in fields if f in ['beta', 'volAvg', 'mktCap', 'lastDiv']]
    sdfields = ['symbol', 'date'] + fields
    ddict = {field: [] for field in sdfields}
    today = str(date.today())
    print('today', today)
    for symbol in symbols:
        url = 'https://financialmodelingprep.com/api/v3/company/profile/' + symbol  # get sector from here
        data = json.loads(urlopen((url)).read().decode("utf-8"))
        # print(data)  # {'symbol': AAP:, 'profile: {'price':, 'beta':, 'volAvg':, 'mktCap:', 'lastDiv':, 'range':,
        #  'changes':, 'changesPercentage':, 'companyName':, 'exchange':, 'industry':, 'website':, 'description':,
        #  'ceo': 'sector':, 'image':}}
        #  industry is within sector (ie, computer hardware is within technology)
        # find_in_json(decoded, 'sector')  # get just the sector, where decoded is rval from the .decode() call
        ddict['symbol'].append(symbol)
        ddict['date'].append(today)
        for field in fields:
            ddict[field].append(data['profile'][field])
    df = pd.DataFrame(ddict)
    for field in numeric_fields:
        df[field] = pd.to_numeric(df[field], errors='coerce')
    # check for NaN
    check = df[numeric_fields].sum()
    if any(check.isna()):
        print('nan detected')
        print(check)
        raise Exception
    return df


if __name__ == '__main__':

    # symbols = get_all_symbols()
    symbols = ['AAPL', 'BA', 'GOOG']
    scratch_p = Path('../../scratch')

    from_date = date(2018, 1, 1)
    to_date = date(2018, 2, 1)

    prices = get_price_history(symbols, from_date=from_date, to_date=to_date)
    print('prices')
    print(prices[:2])
    print(prices[-2:])
    print(prices.shape)
    print(prices.dtypes)
    prices.to_csv(scratch_p / 'prices.csv', index=False)
    print()

    profiles = get_profile(symbols)
    print('profiles')
    print(profiles[:2])
    print(profiles[-2:])
    print(profiles.shape)
    print(profiles.dtypes)
    profiles.to_csv(scratch_p / 'profiles.csv', index=False)

