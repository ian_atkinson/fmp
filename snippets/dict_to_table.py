import sqlalchemy as db

class DictToTable:
    def __init__(self, connection):
        self.connection = connection

    def

def dict_to_table(connection, d, tablename, primary_keys=[], const_cols=[], drop_cols=[], float_cols=[], non_float_cols=[])
    '''
    :param connection: SQLAlchemy connection instance
    :param d: dict: {column_name: list_of_values, ... } 
    :param tablename: string: name of table to be written to
    :param primary_keys: list: primary keys (will avoid duplicating)
    :param const_cols: dict: extra columns with constant values {name: value, ... }
    :param drop_cols: list: don't put these columns into the table
    :param float_cols: list: these columns are numeric
    :param non_float_cols: list; these columns are not numeric
    :return: 
    '''
    columns = const_cols + list(d.keys())
    if not float_cols:
        float_cols = [c for c in columns if c not in non_float_cols]
    sql = 'create table if not exists %s ('
    for column in columns:
        if column in float_cols:
            sql += column + ' float, '
        else:
            sql += column + ' text, '
    sql = sql[:-2] + ');'
    connection.execute(sql)

