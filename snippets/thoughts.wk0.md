thoughts in week 0 (initial setup)

Nicolas' code-block listing all the imports near the start of his article was a good writing technique.  Remember to do this in your articles, and to check that it is complete by a complete dummy-run setup.  

While looking around for how to obtain an API key, I came across another [tutorial](https://github.com/antoinevulcain/Financial-Modeling-Prep-API).

I ran into error message *urllib.error.URLError: <urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: unable to get local issuer certificate (_ssl.c:1108)* when accessing the API.  This was because the Python installation on my Mac lacked the necessary certificates.  This worked, but I think it will apply only to base Python, not in the venv environment that I've set up to work on the project in.  To my surprise the modification to the base environment does flow through to venvs.  Output of the command is in certificates.txt

Current constituents of the S&P500, and additions/deletions going back to July 2000 can be found on [Wikipedia](https://en.wikipedia.org/wiki/List_of_S%26P_500_companies).  I haven't found a similar list for the Russel 3000, although I admit I haven't looked terribly hard.  It may be easier to filter on volume.

Nicolas has put all tickers and indicators into his repository [here](https://github.com/CNIC92/beat-the-stock-market/tree/master/All%20tickers%20and%20Indicators) I've downloaded them too.

We only get five years of history data from the API, so get that.  Set aside a year of data for final verification.

For each stock, take total dollar volume over previous 52 weeks and store in a weekly table (will require week <-> day conversion function).  Group stocks into deciles by liquidity.  Observe performance of buying a random stock in each decile as a strategy.  Determine if delisted stocks are in the database.

Note distribution of missing values in indicator data, and note it for each decile.  Declare a policy around filtering stocks with insufficient indicators (it could be a hyperparameter).

1 year for starting up the volume filter.  1 year at the end for testing.  So 3 years for model building.