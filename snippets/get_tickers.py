from sys import stdout
import numpy as np
import pandas as pd
from pandas_datareader import data
import json

# Reading data from external sources
import urllib as u
from urllib.request import urlopen

# Machine learning (preprocessing, models, evaluation)
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
import xgboost as xgb
from sklearn.metrics import classification_report

# Graphics
from tqdm import tqdm
from functions import get_json_data, find_in_json, get_price_var

url = 'https://financialmodelingprep.com/api/v3/company/stock/list'
ticks_json = get_json_data(url)
available_tickers = find_in_json(ticks_json, 'symbol')
print(len(available_tickers), 'tickers available')
my_tickers = ['AAPL', 'BA', 'GOOG']

tickers_sector = []
for tick in tqdm(my_tickers):
    url = 'https://financialmodelingprep.com/api/v3/company/profile/' + tick # get sector from here
    a = get_json_data(url)
    tickers_sector.append(find_in_json(a, 'sector'))  # is a list

print('len(tickers_sector)', len(tickers_sector), tickers_sector)
print()

S = pd.DataFrame(tickers_sector, index=my_tickers, columns=['Sector'])
print('S')
print(S)
print()

# Get list of tickers from TECHNOLOGY sector
tickers_tech = S[S['Sector'] == 'Technology'].index.values.tolist()
print('tickers_tech')
print(tickers_tech)

pvar_list, tickers_found = [], []
num_tickers_desired = 1000
count = 0
tot = 0
TICKERS = my_tickers

for ticker in TICKERS:
    tot += 1
    try:
        pvar = get_price_var(ticker)
        pvar_list.append(pvar)
        tickers_found.append(ticker)
        count += 1
    except:
        pass

    stdout.write(f'\rScanned {tot} tickers. Found {count}/{len(TICKERS)} usable tickers (max tickets = {num_tickers_desired}).')
    stdout.flush()

    if count == num_tickers_desired: # if there are more than 1000 tickers in sectors, stop
        break

# Store everything in a dataframe
D = pd.DataFrame(pvar_list, index=tickers_found, columns=['2019 PRICE VAR [%]'])
print(D)

# Initialize lists and dataframe (dataframe is a 2D numpy array filled with 0s)
missing_tickers, missing_index = [], []
indicators = open('indicators.txt', 'r').read().splitlines()
d = np.zeros((len(tickers_found), len(indicators)))

for t, _ in enumerate(tqdm(tickers_found)):
    # Scrape indicators from financialmodelingprep API
    url0 = 'https://financialmodelingprep.com/api/v3/financials/income-statement/' + tickers_found[t]
    url1 = 'https://financialmodelingprep.com/api/v3/financials/balance-sheet-statement/' + tickers_found[t]
    url2 = 'https://financialmodelingprep.com/api/v3/financials/cash-flow-statement/' + tickers_found[t]
    url3 = 'https://financialmodelingprep.com/api/v3/financial-ratios/' + tickers_found[t]
    url4 = 'https://financialmodelingprep.com/api/v3/company-key-metrics/' + tickers_found[t]
    url5 = 'https://financialmodelingprep.com/api/v3/financial-statement-growth/' + tickers_found[t]
    a0 = get_json_data(url0)
    # {'symbol': 'AAPL', 'financials': [{'date': 'YYYY-MM-DD', <variaous financials>}, ...]
    a1 = get_json_data(url1)
    a2 = get_json_data(url2)
    a3 = get_json_data(url3)
    a4 = get_json_data(url4)
    a5 = get_json_data(url5)

    # Combine all json files in a list, so that it can be scanned quickly
    A = [a0, a1, a2, a3, a4, a5]
    all_dates = find_in_json(A, 'date')

    check = [s for s in all_dates if '2018' in s]  # find all 2018 entries in dates
    if len(check) > 0:
        date_index = all_dates.index(check[0])  # get most recent 2018 entries, if more are present
        for i, _ in enumerate(indicators):
            ind_list = find_in_json(A, indicators[i])
            try:
                d[t][i] = ind_list[date_index]
            except:
                d[t][i] = np.nan  # in case there is no value inserted for the given indicator

    else:
        missing_tickers.append(tickers_found[t])
        missing_index.append(t)

actual_tickers = [x for x in tickers_found if x not in missing_tickers]
d = np.delete(d, missing_index, 0)
# raw dataset
DATA = pd.DataFrame(d, index=actual_tickers, columns=indicators)
print(DATA)
print(DATA.columns)