import pandas as pd
import requests
from pathlib import Path

fields = ['symbol', 'name', 'lastsale', 'marketcap', 'ipoyear', 'sector', 'industry', 'summaryquote', 'country', 'exchange']
ddict = {field: [] for field in fields}

for country, exchange, url in [
    ['US', 'NASDAQ', 'https://old.nasdaq.com/screening/companies-by-name.aspx?letter=0&exchange=nasdaq&render=download'],
    ['US', 'AMEX', 'https://old.nasdaq.com/screening/companies-by-name.aspx?letter=0&exchange=amex&render=download'],
    ['US', 'NYSE', 'https://old.nasdaq.com/screening/companies-by-name.aspx?letter=0&exchange=nyse&render=download'],
    ['AU', 'ASX', 'http://www.asx.com.au/asx/research/ASXListedCompanies.csv']
]:
    content = requests.get(url).content.decode("utf-8").split(',\r\n')
    print(url)
    for i, row in enumerate(content):
        if len(row) > 0 and i > 0:
            parts = row[1:-1].split('","') + [country, exchange]
            if parts[0].startswith('ZION'):
                print(len(parts), parts, len(fields), fields)
            for j, field in enumerate(fields):
                ddict[field].append(parts[j])

df = pd.DataFrame(ddict)
print(df.shape)
print(df[:2])
p = Path('../../scratch') / 'all_stocks.csv'
df.to_csv(Path('../../scratch') / 'all_stocks.csv', index=False)
