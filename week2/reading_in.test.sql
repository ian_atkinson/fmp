use jjj;

drop table if exists stocks;
go
create table stocks (
	symbol varchar(20),
	sname varchar(100),
	lastsale float,
	marketcap float,
	ipoyear int,
	sector varchar(30),
	industry varchar(100),
	summaryquote varchar(100),
	country varchar(2),
	exchange varchar(6),
	exchange_no int
)
drop table if exists stocks_in;
go
create table stocks_in (
	symbol varchar(20), -- 20
	sname varchar(100),
	lastsale varchar(max),
	marketcap varchar(max),
	ipoyear varchar(4),
	sector varchar(30),
	industry varchar(max),
	summaryquote varchar(max),
	country varchar(2),
	exchange varchar(6)
)
bulk insert stocks_in
from 'C:\Users\family\Documents\Ian\Repos\fmp\scratch\us_stocks.csv'
with (
	format = 'CSV', 
	keepnulls,
	firstrow = 2
);
go

insert into stocks 
select
	left(trim(symbol), 20) as symbol,
	left(trim(sname), 100) as sname,
	try_convert(float, lastsale) as lastsale,
	case 
		when right(marketcap, 1) = 'M' 
			then cast(substring(marketcap,2,len(marketcap)-2) as float) * 1000000.0
			--then '1'
		when right(marketcap, 1) = 'B' 
			then cast(substring(marketcap,2,len(marketcap)-2) as  float) * 1000000000.0
			--then '2'
		when right(marketcap, 1) = 'a' then NULL
		else cast(substring(marketcap,2,len(marketcap)-1) as float) * 1.0
	end as marketcap,
	try_convert(int, ipoyear) as ipoyear,
	case when sector = 'n/a' then NULL else	left(trim(sector), 30) end as sector,
	case when industry  = 'n/a' then NULL else left(trim(industry), 100) end as industry,
	left(trim(summaryquote), 100) as summaryquote,
	left(trim(country), 2) as country,
	left(trim(exchange), 6) as exchange,
	case 
		when exchange = 'NYSE' then 1
		when exchange = 'NASDAQ' then 2
		when exchange = 'AMEX' then 3
		when exchange = 'ASX' then 4
		else NULL
	end as exchange_no	-- this field is not actually necessary here, can put it in the GROUP query (see below),
						-- but I'm keeping it in for clarity here
from stocks_in 
where symbol not like '%^%'
go

-- This almost works, but when resolving stocks that are listed on two exchanges (14 of them in my test data)
-- (same stock, not different stocks with same sname), 
-- the JOIN code generates two rows (duplicate), one per exchange 
-- (the GROUP code works correctly).
-- The commented out bits of code relating to exchange_no was my effort to fix this in the JOIN code.
-- but it doesn't quite work because 
-- it cocks up when the row picked by max_marketcap is different from the row picked up by min_exchange_no.
-- I see no robust solution to this, so I am going to use the GROUP code with the window function.
-- The GROUP code is also a lot shorter and clearer.
select b.* --, a.* 
from
(select sname, max(marketcap) as max_marketcap --, min(exchange_no) as min_exchange_no
	from stocks 
	group by sname 
	having max(marketcap) is not null -- some stocks have no value in this field
) a
left join 
stocks b
on a.sname = b.sname
and a.max_marketcap = b.marketcap
-- and a.min_exchange_no = b.exchange_no
order by b.symbol;

with grouped as (
select 
	row_number() over (
		partition by sname
		--order by marketcap desc, exchange_no asc
		order by 
			marketcap desc,
			case when exchange = 'NYSE' then 1
				when exchange = 'NASDAQ' then 2
				when exchange = 'AMEX' then 3
				when exchange = 'ASX' then 4
				else NULL
			end asc
	) as rown,
	*
from stocks
)
select * from grouped where rown = 1 and marketcap is not null order by symbol;






