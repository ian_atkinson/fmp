import sqlalchemy as db
import pandas as pd
from datetime import date, timedelta
import json
from pathlib import Path
from urllib.request import urlopen
from collections import defaultdict
from date_utils import datetime2week, week2datetimes, week2nysedays, datetime2day

# today = date(year=2020, month=3, day=30)
# for i in range(8):
#     today += timedelta(days=1)
#     print(today, datetime2week(today), today.weekday())
today = date.today()
print('today', today)


class Getter:
    def __init__(self, db_file):
        self.base_url = "https://financialmodelingprep.com/api/v3"
        self.is_Sunday = (date.today().weekday() == 6)
        if not self.is_Sunday:
            db_file = db_file.replace('.db', '_test.db')
            self.week = datetime2week(date.today())
        else:
            self.week = datetime2week(date.today()) - 1
        print(db_file)
        self.engine = db.create_engine(db_file)
        connection = self.engine.connect()

        connection.execute(
            "create table if not exists dates (day int, date text, week int, weekday text, open int, primary key (day));")
        dates = pd.read_sql('dates', connection)
        print('have read dates', dates)
        if len(dates['week']) == 0:
            new_weeks = [self.week]
        else:
            new_weeks = range(max(dates['week']) + 1, self.week + 1)
        print('new_weeks', new_weeks)
        for week in new_weeks:
            dates = week2datetimes(week)
            open_days = week2nysedays(week)
            for date_thing in dates:  # bc 'for date in ...' triggers weird namespace collision with 'import date'
                day = datetime2day(date_thing)
                sql = "insert into %s (day, date, week, weekday, open) values (%d, '%s', %d, '%s', %d)" % \
                      (
                      "dates", day, date_thing.strftime('%Y-%m-%d'), week, date_thing.strftime("%a"), date in open_days)
                print(sql)
                connection.execute(sql)
        self.got_date = date.today().strftime('%Y-%m-%d')
        self.got_day = datetime2day(date.today())
        self.got_week = datetime2week(date.today())
        print('got_date', self.got_date, 'got_day', self.got_day)

    def get_all_symbols(self, subset=None):
        # returns a list of all stock codes, etc., in the fmp database
        url = 'https://financialmodelingprep.com/api/v3/company/stock/list'
        symbols = self.find_in_json(json.loads(urlopen(url).read().decode('utf-8')), 'symbol')
        if type(subset) == int:
            symbols = symbols[:subset]
        elif type(subset) == list:
            symbols = [s for s in subset if s in symbols]
        print('gotten', len(symbols), 'symbols')
        return symbols

    def get_from_url(self, url):
        response = urlopen(url)
        json_data = response.read().decode('utf-8')
        data = json.loads(json_data)
        return data

    def find_in_json(self, obj, key):
        '''
        Scan the json file to find the value of the required key.
        Input: json file
               required key
        Output: value corresponding to the required key
        '''
        # Initialize output as empty
        arr = []

        def extract(obj, arr, key):
            '''
            Recursively search for values of key in json file.
            '''
            if isinstance(obj, dict):
                for k, v in obj.items():
                    if isinstance(v, (dict, list)):
                        extract(v, arr, key)
                    elif k == key:
                        arr.append(v)
            elif isinstance(obj, list):
                for item in obj:
                    extract(item, arr, key)
            return arr

        results = extract(obj, arr, key)
        return results

    def get_ratios(self, symbols):
        datadict = defaultdict(list)
        url = self.base_url + '/financial-ratios/'
        for symbol in symbols:
            data = self.get_from_url(url + symbol)['ratios']
            for i, datum in enumerate(data):
                done_ratios = []  # bc some ratios appear in many groups
                # print(i, 'have reset done_ratios')
                for j, key in enumerate(datum):
                    if key == 'date':
                        datadict['date'].append(datum['date'])
                        datadict['day'].append(datetime2day(datum['date']))
                        datadict['week'].append(datetime2week(datum['date']))
                    else:
                        ratio_group = key
                        for k, ratio in enumerate(datum[ratio_group]):
                            if ratio == 'niperEBT':
                                # this one appears in one place as niperEBT and as nIperEBT in another
                                continue
                            if ratio.lower() not in done_ratios:
                                datadict[ratio].append(datum[ratio_group][ratio])
                                done_ratios.append(ratio.lower())
                                # print(i, j, k, 'have appended', ratio, 'to done_ratios', len(done_ratios))
                            else:
                                # print(i, j, k, 'dup ratio', ratio)
                                pass
                datadict['symbol'].append(symbol)

        # correct a data glitch (see ~/fmp/correspondence/*)
        rt = datadict['receivablesTurnover']
        lendate = len(datadict['date'])
        if len(rt) != lendate:
            n = int(len(rt) / lendate)
            new_rt = rt[::n]
            print('correcting receivablesTurnover', len(rt), len(new_rt))
            datadict['receivablesTurnover'] = new_rt

        df = pd.DataFrame(datadict)

        # put in metadata (got_day, got_date)
        df['got_day'] = self.got_day
        df['got_date'] = self.got_date
        df['got_week'] = self.got_week

        # make numeric columns numeric
        numeric_columns = [c for c in list(df.columns) if c not in ['date', 'symbol', 'got_date']]
        df[numeric_columns] = df[numeric_columns].apply(pd.to_numeric, errors='raise')
        # not errors='coerce' bc I want to know about these errors, for now

        # reorder cols to be nice
        first_columns = ['symbol', 'date', 'day', 'week', 'got_date', 'got_day', 'got_week']
        ordered_columns = first_columns + [c for c in list(df.columns) if c not in first_columns]
        df = df[ordered_columns]

        return df

    def get_prices(self, symbols):
        datadict = defaultdict(list)
        url = self.base_url + '/historical-price-full/'
        for symbol in symbols:
            data = self.get_from_url(url + symbol)['historical']
            print(data)
            for datum in data:
                for column in datum:
                    if column == 'date':
                        datadict['date'].append(datum['date'])
                        datadict['week'].append(datetime2week(datum['date']))
                        datadict['day'].append(datetime2day(datum['date']))
                    else:
                        datadict[column].append(datum[column])
                datadict['symbol'].append(symbol)
        df = pd.DataFrame(datadict)
        df.to_csv('prices.csv', index=False)

        # put in metadata
        df['got_day'] = self.got_day
        df['got_date'] = self.got_date
        df['got_week'] = self.got_week

        # make numeric columns mumeric
        numeric_columns = [c for c in list(df.columns) if c not in ['symbol', 'date', 'label', 'got_date']]
        df[numeric_columns] = df[numeric_columns].apply(pd.to_numeric, errors='raise')

        # reorder cols to be nice
        first_columns = ['symbol', 'date', 'day', 'week', 'got_date', 'got_day', 'got_week']
        ordered_columns = first_columns + [c for c in list(df.columns) if c not in first_columns]
        df = df[ordered_columns]

        return df

    def get_profiles(self, symbols):
        datadict = defaultdict(list)
        url = self.base_url + '/company/profile/'
        for symbol in symbols:
            data = self.get_from_url(url + symbol)['profile']
            print(data)
            for column in ['price', 'beta', 'volAvg', 'mktCap', 'lastDiv', 'industry', 'sector', 'companyName']:
                # don't want all the fields.  Note that there is no date field, current profile only
                datadict[column].append(data[column])
            datadict['symbol'] = symbol
        df = pd.DataFrame(datadict)
        df.to_csv('profiles.csv', index=False)

        # put in metadata
        df['got_day'] = self.got_day
        df['got_date'] = self.got_date
        df['got_week'] = self.got_week

        # make numeric columns mumeric
        numeric_columns = [c for c in list(df.columns) if c not in
                           ['symbol', 'industry', 'sector', 'companyName', 'got_date']]
        df[numeric_columns] = df[numeric_columns].apply(pd.to_numeric, errors='raise')

        # reorder cols to be nice
        first_columns = ['symbol', 'got_date', 'got_day', 'got_week']
        ordered_columns = first_columns + [c for c in list(df.columns) if c not in first_columns]
        df = df[ordered_columns]

        return df


    def get_ratio_groups(self, csv_p, symbol='AAPL'):
        # is only done when setting up the database, to create a .csv file showing what groups each ratio belongs in
        # a few ratios are placed in several groups
        # This method remains as a way to produce human-readable doc, may become redundant
        rdict = {'ratio_group': [], 'ratio': []}
        url = self.base_url + '/financial-ratios/' + symbol
        datum = self.get_from_url(url)['ratios'][0]
        for key in datum:
            if key == 'date':
                pass
            else:
                ratio_group = key
                for ratio in datum[ratio_group]:
                    rdict['ratio_group'].append(ratio_group)
                    rdict['ratio'].append(ratio)
        df = pd.DataFrame(rdict)
        df.to_csv(csv_p, index=False)

    def to_db(self, df, table_name, drop_table=False):
        connection = self.engine.connect()
        if drop_table:
            connection.execute("drop table if exists %s" % table_name)
        print(list(df.columns))
        df.to_sql(table_name, connection, if_exists='append', index=False)




if __name__ == '__main__':
    getter = Getter('sqlite:///../../sqlite/fmp2.db')
    #getter.get_ratio_groups('groups.csv')

    df = getter.get_profiles(['AAPL', 'BA', 'GOOG'])

    df = getter.get_prices(['AAPL', 'BA', 'GOOG'])
    print(df.shape)
    df.to_csv(Path('../../scratch') / 'prices.csv', index=False)
    #getter.to_db(df, 'prices', drop_table=True)

    # symbols = getter.get_all_symbols(subset=['AAPL', 'BA', 'GOOG'])
    # df = getter.get_ratios(symbols)
    # print(df.shape)
    # df.to_csv('ratios.csv', index=False)
    # getter.to_db(df, 'ratios', drop_table=True)



