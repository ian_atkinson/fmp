use jjj;

drop table if exists stocks_in;
go
create table stocks_in (
	symbol varchar(20), -- 20
	sname varchar(100),
	lastsale varchar(max),
	marketcap varchar(max),
	ipoyear varchar(4),
	sector varchar(30),
	industry varchar(max),
	summaryquote varchar(max),
	country varchar(2),
	exchange varchar(6)
);

drop table if exists stocks;
go
create table stocks (
	symbol varchar(20),
	sname varchar(100),
	lastsale float,
	marketcap float,
	ipoyear int,
	sector varchar(30),
	industry varchar(100),
	summaryquote varchar(100),
	country varchar(2),
	exchange varchar(6)
);

bulk insert stocks_in
from 'C:\Users\family\Documents\Ian\Repos\fmp\scratch\us_stocks.csv'
with (
	format = 'CSV', 
	keepnulls,
	firstrow = 2
);
go
