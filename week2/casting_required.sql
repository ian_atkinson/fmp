/*
LEARNING
strings (and expressions that return strings) 
will automatically/implicitly be cast to int when required by arithmetic
but must be explitly be cast to float when a float is required by arithmetic.

"when in doubt, cast" is probably the prudent approach?
*/

use jjj;
drop table if exists scratchy;
create table scratchy (
	symbol varchar(3),
	toconvert varchar(5)
);
insert into scratchy (symbol, toconvert) values 
	('AAA', '10000'),
	('BBB', '10B');

select  
	symbol, 
	toconvert, 
	case right(toconvert, 1)  
		when 'B' then left(toconvert, len(toconvert) - 1)
		else toconvert
	end as truncated,
	case right(toconvert, 1)  
		when 'B' then left(toconvert, len(toconvert) - 1) * 1
		else toconvert * 1
	end as timesone,
	--case right(toconvert, 1)  
	--	when 'B' then left(toconvert, len(toconvert) - 1) * 1.0
	--	else toconvert * 1.0
	--end as timesonepointoh
	case right(toconvert, 1)  
		when 'B' then cast(left(toconvert, len(toconvert) - 1) as float) * 1.0
		else cast(toconvert as float) * 1.0
	end as castfloat_timesonepointoh
	from scratchy;