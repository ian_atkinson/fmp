import requests
import string
from pathlib import Path
import json
from urllib.request import urlopen
from functions import find_in_json
import pandas as pd

def get_symbols():

    # S&P500
    sp500_symbols = []
    url = 'https://en.wikipedia.org/wiki/List_of_S%26P_500_companies'
    text = requests.get(url).text
    text = text.split('S&amp;P 500 component stocks')[3]
    text = text.split('</td></tr></tbody></table>')[0]
    for i, line in enumerate(text.split('\n')):
        if line.startswith('<td><a rel="nofollow" class="external text" href=') and line.endswith('</a>'):
            sp500_symbols.append(line.split('">')[1][:-4])
    print('S&P500', len(sp500_symbols))

    # nyse
    nyse_symbols = []
    for c in string.ascii_uppercase:
        url = 'https://www.advfn.com/nyse/newyorkstockexchange.asp?companies=%s' % c
        text = requests.get(url).text
        text = text.split('Companies listed on the NYSE')[1]
        text = text.split('<tr><th>Equity</th><th>Symbol</th><th>Info</th></tr>')[1]
        text = text.split('</table>')[0]
        nyse_symbols += [line.split('NYSE')[1].split('/stock-price')[0].split('-')[-1]
                         for line in text.split('\n') if 'NYSE' in line]
    print('NYSE', len(nyse_symbols))

    # nasdaq
    nasdaq_symbols = []
    for c in string.ascii_uppercase:
        url = 'https://www.advfn.com/nasdaq/nasdaq.asp?companies=%s' % c
        text = requests.get(url).text
        text = text.split('Companies listed on the NASDAQ')[1]
        text = text.split('<tr><th>Equity</th><th>Symbol</th><th>Info</th></tr>')[1]
        text = text.split('</table>')[0]
        nasdaq_symbols += [line.split('NASDAQ')[1].split('/stock-price')[0].split('-')[-1]
                           for line in text.split('\n') if 'NASDAQ' in line]
    print('NASDAQ', len(nasdaq_symbols))

    # amex
    amex_symbols = []
    for c in string.ascii_uppercase:
        url = 'https://www.advfn.com/amex/americanstockexchange.asp?companies=A'
        text = requests.get(url).text
        text = text.split('Companies listed on the AMEX')[1]
        text = text.split('<tr><th>Equity</th><th>Symbol</th><th>Info</th></tr>')[1]
        text = text.split('</table>')[0]
        amex_symbols += [line.split('AMEX')[1].split('/stock-price')[0].split('-')[-1]
                         for line in text.split('\n') if 'AMEX' in line]
    print('AMEX', len(amex_symbols))

    # how many distinct stocks?
    print('all stocks', len(set(nyse_symbols + nasdaq_symbols + amex_symbols)))

    # in the fmp database
    url = 'https://financialmodelingprep.com/api/v3/company/stock/list'
    fmp_symbols = find_in_json(json.loads(urlopen(url).read().decode('utf-8')), 'symbol')

    all_symbols = list(set(sp500_symbols + nyse_symbols + nasdaq_symbols + amex_symbols + fmp_symbols))
    all_symbols = [[s, s in fmp_symbols, s in sp500_symbols, s in nyse_symbols, s in nasdaq_symbols, s in amex_symbols]
                   for s in all_symbols]
    print('all', len(all_symbols))
    df = pd.DataFrame(all_symbols, columns=['symbol', 'fmp', 'sp500', 'nyse', 'nasdaq', 'amex'])
    df['fmp_stock'] = df['fmp'] & (df['nyse'] | df['nasdaq'] | df['amex'])
    df.to_csv(Path('../../scratch') / 'df_bool.csv', index=False)
    for col in ['fmp', 'sp500', 'nyse', 'nasdaq', 'amex', 'fmp_stock']:
        df[col] = df[col].astype(int)
    df.to_csv(Path('../../scratch') / 'df_numb.csv', index=False)

    return df

if __name__ == '__main__':
    df = get_symbols()
    print(df)
