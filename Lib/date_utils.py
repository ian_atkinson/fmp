from datetime import datetime, timedelta, date
import pandas_market_calendars as mcal


def todate(s):
    if type(s) == str:
        return datetime.strptime(s, '%Y-%m-%d')
    elif type(s) == date:
        return datetime.strptime(str(s), '%Y-%m-%d')
    # if type(s) == pandas.timestamp:
    #     return s.date()
    return s


def datetime2day(date_time):
    return (todate(date_time) - datetime(1970, 1, 4)).days


def datetime2week(date_time):
    # week starts on Sunday for this function
    # Sunday is day 6 in python
    return int(datetime2day(date_time) / 7)


def day2datetime(day):
    return datetime(1970, 1, 4) + timedelta(days=day)


def week2datetimes(week):
    return [day2datetime(week * 7 + i + 1) for i in range(5)]


def week2monday(week):
    return day2datetime(week * 7 + 1)


def week2nysedays(week):
    days = week2datetimes(week)
    nyse = mcal.get_calendar('NYSE').schedule(days[0].strftime('%Y-%m-%d'), days[-1].strftime('%Y-%m-%d'))
    return [d.to_pydatetime() for d in nyse.index]




'''
For historical == True (the only case I'm considering right now)
to support, say, week 2000 with max(lookback) = 4 and max(target) = 2 and indicator_lag = 3 we require:
1. liquidity from week 2000 - 2 - 4 = 1995 to week 2000 - 1 = 1999
2. data from week 2000 - 2 - 4 - 3 = 1995 to week 2000 + 2 = 2002
After building our features and targets, we will use
1. training data from week 2000 - 2 - 4 = 1994 to 2000 - 2 - 1 = 1997
3. test data of week 2000
'''


def get_required_weeks(sup_week, historical, max_lookback, max_target, indicator_lag):
    #  this is the "data from ... 1995 to ... 2002" bit in the comment above
    if historical:
        first_req_week = sup_week - max_lookback - max_target - indicator_lag
        last_req_week = sup_week + max_target
    else:
        raise Exception("haven't written this code yet")
    return first_req_week, last_req_week


def get_liquidity_weeks(sup_week, historical, max_lookback, max_target):
    #  this is the "liquidity from ... 2995 to ... 1999" bit
    if historical:
        first_liq_week = sup_week - max_lookback - max_target
        last_liq_week = sup_week - 1
    else:
        raise Exception("haven't written this code yet")
    return first_liq_week, last_liq_week


def get_training_weeks(sup_week, historical, max_lookback, max_target):
    if historical:
        first_req_week = sup_week - max_lookback - max_target
        last_req_week = sup_week - max_target - 1
    else:
        raise Exception("haven't written this code yet")
    return first_req_week, last_req_week


if __name__ == '__main__':
    print(day2datetime(3))
    print(datetime2day('1970-01-07'))
    print(week2datetimes(0))