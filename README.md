# Financial Modeling Prep

Exploring the [article](https://towardsdatascience.com/beat-the-stock-market-with-machine-learning-d9432ea5241e) by [Nicolas Carbone](https://towardsdatascience.com/@nicolascarbone92)  and the Financial Modeling Prep data [API](https://financialmodelingprep.com/developer/docs/) that the article references.

Intention is to focus more on business metrics and outcomes around building an investing system that works in terms of risk and reward rather than scrutinising ML algorithms in detail, although significant consideration of ML is expected.

